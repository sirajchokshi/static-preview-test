let interval = null;
let seed = Math.random() * 100000;

function render() {
  WebAssembly.instantiateStreaming(fetch("./assets/noise.wasm")).then(
    (prog) => {
      const perlin2d = prog.instance.exports.perlin_2d;

      if (!perlin2d) {
        console.log("Error: perlin2d is null. Bad WASM binary.");
        return;
      }

      const canvas = document.getElementById("canvas");
      const ctx = canvas.getContext("2d");
      const imageData = ctx.createImageData(canvas.width, canvas.height);
      const data = imageData.data;

      const width = canvas.width;
      const height = canvas.height;

      function paint() {
        seed = Math.random() * 100000;

        for (let y = 0; y < height; y++) {
          for (let x = 0; x < width; x++) {
            const i = (y * width + x) * 4;
            const noise = prog.instance.exports.perlin_2d(
              x / 125000,
              y / 100000,
              seed,
              1
            );

            data[i] = noise % 255;
            data[i + 1] = noise % 255;
            data[i + 2] = noise % 255;
            data[i + 3] = 255;
          }
        }

        ctx.putImageData(imageData, 0, 0);
      }

      paint();
    }
  );
}

// ui
const btn = document.getElementById("render-btn");

function pause() {
  if (interval) {
    window.clearInterval(interval);
    interval = null;
    btn && (btn.innerText = "Play");
  }
}

function play() {
  if (!interval) {
    interval = window.setInterval(render, 100);
    render();
    btn && (btn.innerText = "Pause");
  }
}

// init

btn.addEventListener("click", () => (interval ? pause() : play()));

play();

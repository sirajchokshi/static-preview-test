// DEBUG
if (!window.log) {
  window.log = function (pass, ...args) {
    if (typeof pass === "boolean") {
      pass = pass ? "(PASS)" : "(FAIL)";
    }

    logger("[DEBUG:]", pass, ...args);
  };

  // if window.log isn't defined yet, the inline script failed
  log(false, "Inline script");
}

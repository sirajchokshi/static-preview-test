import { h, render } from "https://esm.sh/preact";

const app = h(
  "main",
  {
    style: {
      textAlign: "center",
    },
  },
  [
    h(
      "div",
      {
        style: {
          marginTop: "6rem",
          height: "6rem",
        },
        innerHTML: `
        <svg width="100%" height="100%" viewBox="-256 -256 512 512" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve">
          <path d="M0,-256 221.7025033688164,-128 221.7025033688164,128 0,256 -221.7025033688164,128 -221.7025033688164,-128z" fill="white"/>
          <ellipse cx="0" cy="0" stroke-width="16px" rx="75px" ry="196px" fill="none" stroke="#673ab8" transform="rotate(52.5)"/>
          <ellipse cx="0" cy="0" stroke-width="16px" rx="75px" ry="196px" fill="none" stroke="#673ab8" transform="rotate(-52.5)"/>
          <circle cx="0" cy="0" r="34" fill="#673ab8"/>
        </svg>`,
      },
      null
    ),
    h(
      "h1",
      {
        style: {
          margin: "0.5rem 0",
        },
      },
      "Hello World!"
    ),
    h("p", null, "This page uses Preact to render content on the client."),
  ]
);

render(app, document.body);
